extends Area3D

@export var bullet_base_speed : float = 200
@export var inherited_velocity : Vector3 = Vector3(0, 0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start(5)

func _physics_process(delta):
	translate((Vector3(0, 0, -bullet_base_speed) + inherited_velocity) * delta)
	
	var bodies = get_overlapping_bodies()
	for i in bodies.size():
		if bodies[i].is_in_group("enemy"):
			bodies[i].queue_free()


func _on_Timer_timeout():
	queue_free()
