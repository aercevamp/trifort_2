extends CanvasLayer

var stats = []
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func add_stat(stat_name: String, object: Object, stat_ref: String, is_method: bool):
	stats.append([stat_name, object, stat_ref, is_method])

func get_text() -> String:
	var label_text = ""
	
	var fps = Engine.get_frames_per_second()
	label_text += str("FPS: ", fps, "\n")
	
	for s in stats:
		var value = null
		if s[1] and weakref(s[1]).get_ref():
			if s[3]:
				value = s[1].call(s[2])
			else:
				value = s[1].get(s[2])
		label_text += str(s[0], ": ", value)
		label_text += "\n"
	return label_text
