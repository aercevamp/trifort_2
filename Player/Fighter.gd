extends CharacterBody3D


@export var max_speed = 10.0 
@export var min_speed = 1.0 #negative min speed for max reverse speed?
@export var acceleration = 0.1
@export var pitch_speed = 0.5
@export var roll_speed =  0.5
@export var yaw_speed = 0.1
@export var input_response = 8.0

@onready var bullet = preload("res://bullet.tscn")

var forward_speed = 0
var pitch_input = 0.0
var roll_input = 0.0
var yaw_input = 0.0
var new_speed = 0.0
var pitch_momentum = 0.0
var roll_momentum = 0.0
var yaw_momentum = 0.0
var thrust_input = 0.0

#gravity & stuff for ground?

func _physics_process(delta):
	#ROTATION ORDER MATTERS!!!
	pitch_input = (Input.get_action_strength("pitch_up") - Input.get_action_strength("pitch_down"))
	roll_input = (Input.get_action_strength("roll_left") - Input.get_action_strength("roll_right"))
	yaw_input = (Input.get_action_strength("yaw_left") - Input.get_action_strength("yaw_right"))
	thrust_input = (Input.get_action_strength("throttle_up") - Input.get_action_strength("throttle_down"))
	
	if Input.is_action_just_pressed("shoot"): shoot()
	#PITCH
	if pitch_input != 0:
		pitch_momentum = clamp(pitch_momentum + pitch_input * pitch_speed * 0.1, -2, 2)
	else:
		var pitch_direction = sign(pitch_momentum)
		pitch_momentum -= pitch_direction * pitch_speed * 0.12
		if abs(pitch_momentum) < 0.05: #it being here might make it "snap" to 0 more than it would normally but eh
			pitch_momentum = 0
	
	#roll
	if roll_input != 0:
		roll_momentum = clamp(roll_momentum + roll_input * roll_speed * 0.1, -3, 3)
	else:
		var roll_direction = sign(roll_momentum)
		roll_momentum -= roll_direction * roll_speed * 0.12
		if abs(roll_momentum) < 0.1: #it being here might make it "snap" to 0 more than it would normally but eh
			roll_momentum = 0
	
		#yaw
	if yaw_input != 0:
		yaw_momentum = clamp(yaw_momentum + yaw_input * yaw_speed * 0.1, -1, 1)
	else:
		var yaw_direction = sign(yaw_momentum)
		yaw_momentum -= yaw_direction * yaw_speed * 0.12
		if abs(yaw_momentum) < 0.1: #it being here might make it "snap" to 0 more than it would normally but eh
			yaw_momentum = 0

	#THRUST
	new_speed = forward_speed + (thrust_input * acceleration)
	forward_speed = clamp(new_speed, min_speed, max_speed)
	#Thrust needs diff acceleration and breaking speeds
	#Also have roll and pitch have some momentum? can't reverse instantly
	
	transform.basis = transform.basis.rotated(transform.basis.z, roll_momentum * delta)
	transform.basis = transform.basis.rotated(transform.basis.x, pitch_momentum * delta)
	transform.basis = transform.basis.rotated(transform.basis.y, yaw_momentum * delta)
	transform.basis = transform.basis.orthonormalized()
	velocity = -transform.basis.z * forward_speed
	move_and_slide()

func shoot():
	var bullet_instance = bullet.instantiate()
	get_parent().add_child(bullet_instance)
	bullet_instance.global_transform = self.global_transform
	bullet_instance.transform.origin = self.transform.origin
	bullet_instance.inherited_velocity = Vector3(velocity.x, 0, 0) #for making the bullet get the spaceship's speed too
