extends CharacterBody3D

var vel : Vector3
var rot_vel : Vector3
var yaw_vel : Vector3
var pitch_vel : Vector3
var roll_vel : Vector3

var thrust_const : float = 20
var yaw_const : float = 1.2
var pitch_const : float = 1.5
var roll_const : float = 2.0

var max_thrust_forw : float = 200
var max_thrust_backw : float = 50
var min_thrust_forw : float = 50
var min_thrust : float = 0.1
var min_rot_vel : float = 0.05
var max_yaw : float = 1.5
var max_pitch : float = 2.0
var max_roll : float = 2.0

var thrust_forw : bool
var thrust_backw : bool
var yaw_left : bool
var yaw_right : bool
var pitch_up : bool
var pitch_down : bool
var roll_left : bool
var roll_right : bool
var shoot : bool
var shoot_extended : bool

var camera_switch : bool
var crr_camera : int = 1

var laser_num = 0
var laser_continuous : bool = false
var time_since_laser : float = 0
var laser_interval : float = 0.1

var life = 100

var enemy_count : int



# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	

func _physics_process(delta):
	
	#movement inputs
	thrust_forw = true if Input.is_action_pressed("throttle_up") else false
	thrust_backw = true if Input.is_action_pressed("throttle_down") else false
	pitch_up = true if Input.is_action_pressed("pitch_up") else false
	pitch_down = true if Input.is_action_pressed("pitch_down") else false
	roll_left = true if Input.is_action_pressed("roll_left") else false
	roll_right = true if Input.is_action_pressed("roll_right") else false
	#shoot inputs
	shoot = true if Input.is_action_just_pressed("shoot") else false
	shoot_extended = true if Input.is_action_pressed("shoot") else false
	
	#thrust
	var thrust_dot = vel.dot(Vector3(-1, 0, 0))
	if thrust_forw or thrust_backw:
		if thrust_forw:
			if thrust_dot < 0:
				vel += Vector3(-1, 0, 0) * thrust_const * 2.5 * delta
			elif thrust_dot < max_thrust_forw:
				vel += Vector3(-1, 0, 0) * thrust_const * delta
		if thrust_backw:
			if thrust_dot > 0:
				vel += Vector3(1, 0, 0) * thrust_const * 2.5 * delta
			elif thrust_dot > -max_thrust_backw:
				vel += Vector3(1, 0, 0) * thrust_const * delta
	else:
		if thrust_dot >= min_thrust_forw:
			pass
		elif thrust_dot > min_thrust and thrust_dot < min_thrust_forw:
			vel += Vector3(1, 0, 0) * thrust_const * 1.5 * delta
		elif thrust_dot < -min_thrust:
			vel += Vector3(-1, 0, 0) * thrust_const * 1.5 * delta
		else:
			vel.x = 0
	
	#yaw
	var yaw_dot = yaw_vel.dot(Vector3(0, 1, 0))
	if yaw_left or yaw_right:
		if yaw_left:
			if yaw_dot < 0:
				yaw_vel += Vector3(0, 1, 0) * 2 * yaw_const * delta
			if yaw_dot < max_yaw:
				yaw_vel += Vector3(0, 1, 0) * yaw_const * delta
		if yaw_right:
			if yaw_dot > 0:
				yaw_vel += Vector3(0, -1, 0) * yaw_const * 2 * delta
			elif yaw_dot > -max_yaw:
				yaw_vel += Vector3(0, -1, 0) * yaw_const * delta
	else:
		if yaw_dot > min_rot_vel:
			yaw_vel -= Vector3(0, 1, 0) * yaw_const * 1.5 * delta
		elif yaw_dot < -min_rot_vel:
			yaw_vel -= Vector3(0, -1, 0) * yaw_const * 1.5 * delta
		else:
			yaw_vel = Vector3()
			vel.z = 0
	
	#pitch
	var pitch_dot = pitch_vel.dot(Vector3(0, 0, -1))
	if pitch_up or pitch_down:
		if pitch_up:
			if pitch_dot < 0:
				pitch_vel += Vector3(0, 0, -1) * pitch_const * 3 * delta
			elif pitch_dot < max_pitch:
				pitch_vel += Vector3(0, 0, -1) * pitch_const * delta
		if pitch_down:
			if pitch_dot > 0:
				pitch_vel += Vector3(0, 0, 1) * pitch_const * 3 * delta
			elif pitch_dot > -max_pitch:
				pitch_vel += Vector3(0, 0, 1) * pitch_const * delta
	else:
		if pitch_dot > min_rot_vel:
			pitch_vel -= Vector3(0, 0, -1) * pitch_const * 1.5 * delta
		elif pitch_dot < -min_rot_vel:
			pitch_vel -= Vector3(0, 0, 1) * pitch_const * 1.5 * delta
		else:
			pitch_vel = Vector3()
			vel.y = 0
	
	#roll
	var roll_dot = roll_vel.dot(Vector3(1, 0, 0))
	if roll_left or roll_right:
		if roll_left:
			if roll_dot < 0:
				roll_vel += Vector3(1, 0, 0) * roll_const * 2 * delta
			elif roll_dot < max_roll:
				roll_vel += Vector3(1, 0, 0) * roll_const * delta
		if roll_right:
			if roll_dot > 0:
				roll_vel += Vector3(-1, 0, 0) * roll_const * 2 * delta
			elif roll_dot > -max_roll:
				roll_vel += Vector3(-1, 0, 0) * roll_const * delta
	else:
		if roll_dot > min_rot_vel:
			roll_vel -= Vector3(1, 0, 0) * roll_const * 1.5 * delta
		elif roll_dot < -min_rot_vel:
			roll_vel -= Vector3(-1, 0, 0) * roll_const * 1.5 * delta
		else:
			roll_vel = Vector3()
	
	#apply movements
	rot_vel = yaw_vel + pitch_vel + roll_vel
	vel += rot_vel.cross(vel) * delta * delta
	if rot_vel.length() != 0:
		rotate(transform.basis * (rot_vel.normalized()), rot_vel.length() * delta)
	

