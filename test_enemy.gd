extends CharacterBody3D

var player_direction : Vector3 = Vector3(0, 0, 0)
var player : Object = null
func _ready():
	player = get_parent().get_node("Fighter")

func _physics_process(delta):
	look_at(player.position)
